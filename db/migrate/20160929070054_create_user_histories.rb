class CreateUserHistories < ActiveRecord::Migration
  def change
    create_table :user_histories do |t|
      t.belongs_to :user, :limit => 8
      t.integer :version_no
      t.string :name
      t.integer :total_amount
      t.integer :paid_amount

      t.timestamps
    end
  end
end
