class AddAddittionalColsToUser < ActiveRecord::Migration
  def change
  	add_column :users, :name, :string 
    add_column :users, :total_amount, :integer
    add_column :users, :paid_amount, :integer
  end
end
