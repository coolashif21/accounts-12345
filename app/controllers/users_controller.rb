class UsersController < ApplicationController
  before_action :set_user, only: [:show, :edit, :update, :destroy]

  # GET /users
  # GET /users.json
  def index
    @users = User.all
  end

  # GET /users/1
  # GET /users/1.json
  def show
    @user = User.find(params[:id])
    if params[:timestamp]
      params[:timestamp] = Time.at(params[:timestamp].to_i)
      @user_histories = UserHistory.where(:user_id => params[:id]).where(:created_at => params[:timestamp])
    else   
      @user_histories = UserHistory.where(:user_id => params[:id])
    end
  end

  # GET /users/new
  def new
    @user = User.new
  end

  # GET /users/1/edit
  def edit
  end

  # POST /users
  # POST /users.json
  def create
    @user = User.new(user_params)
    @user_history = UserHistory.new(user_params)
    @user_history.version_no = 1
    @user.user_history.push @user_history
    respond_to do |format|
      if @user.save
        format.html { redirect_to @user, notice: 'User was successfully created.' }
        format.json { render :show, status: :created, location: @user }
      else
        format.html { render :new }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /users/1
  # PATCH/PUT /users/1.json
  def update
    respond_to do |format|
      if @user.update(user_params)
        @last_revision_no = UserHistory.where(:user_id => params[:id]).last(1)[0].version_no
        user_history = UserHistory.new(user_params)
        user_history.version_no = @last_revision_no + 1
        user_history.user_id = params[:id]
        user_history.save  
        format.html { redirect_to @user, notice: 'User was successfully updated.' }
        format.json { render :show, status: :ok, location: @user }
      else
        format.html { render :edit }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    @user.destroy
    respond_to do |format|
      format.html { redirect_to users_url, notice: 'User was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      permitted = params.require(:user).permit(:name, :total_amount, :paid_amount)
      permitted
    end
end
#params.require(:user).permit(:name, :total_amount, :paid_amount)